import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { Router } from '@angular/router';


import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signup:FormGroup
  isSubmit: boolean = false;

  constructor(private http:HttpService, private route:Router, private fb: FormBuilder ) { }

  ngOnInit() {
    this.signup = this.fb.group({
      'email': new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      'password': new FormControl('', [Validators.required])
    })
  }
  // Function for sign-up button
  signupForm(){
    this.isSubmit = true;
    console.log(this.signup.value);
    if (this.isSubmit && this.signup.valid) {
      var data = {
        "email": this.signup.value.email,
        "password": this.signup.value.password
      }
      this.http.httpPost('signup', data).subscribe((res: any) => {
        this.route.navigateByUrl("/auth/login");

      })
    }
  }
}




       

       
        

   