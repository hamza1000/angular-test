import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { Router } from '@angular/router';



import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { parse } from 'querystring';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isSubmit: boolean;
  isChcked:boolean = false;

  constructor(private http:HttpService, private route:Router, private fb: FormBuilder ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      'email': new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      'password': new FormControl('', [Validators.required])

    })
    if(localStorage.credentials){
      var data = localStorage.credentials ? JSON.parse(localStorage.credentials) : ''
      this.loginForm.patchValue({
        "email":data.email,
        "password":data.password,

      })
      this.isChcked = true;
    }
  }
  rememberMe1(e){
    console.log("working");
    if(e.target.checked && this.loginForm.valid){
      
      var loginCred ={
        email:this.loginForm.value.email,
        password :this.loginForm.value.password
      }
      localStorage.setItem('credentials', JSON.stringify(loginCred))

    }else if(!e.target.checked){
      localStorage.removeItem('credentials')
    }

  }
  
  // Function for login form
  login(){
    this.isSubmit = true;
    console.log(this.loginForm.value);
    if (this.isSubmit && this.loginForm.valid) {
      var data = {
        "email": this.loginForm.value.email,
        "password": this.loginForm.value.password
      }
      this.http.httpPost('login', data).subscribe((res: any) => {
        localStorage.setItem('accessToken', JSON.stringify(res.data.token));
        this.route.navigateByUrl("/pages/dashboard");
        
      })
    }}

}

        
        




       

       
        
