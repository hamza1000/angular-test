import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { SignupComponent } from './signup/signup.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpService } from '../services/http.service';
import { apiList } from '../services/api-list';
import {MatButtonModule} from '@angular/material/button';
import { LoginComponent } from './login/login.component';




@NgModule({
  declarations: [AuthComponent, SignupComponent, LoginComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonModule,
   
  ],
  providers: [apiList, HttpService]
})

export class AuthModule { }
