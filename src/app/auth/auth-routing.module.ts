import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';

import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardGuard } from '../services/auth-guard.guard'

const routes: Routes = [
{ path: 'signup', component: SignupComponent},
{path: 'login', component: LoginComponent}
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
