import { getAllLifecycleHooks } from '@angular/compiler/src/lifecycle_reflector';
import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import * as data from '../../../assets/properties.json';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  buildingData:any[]=[];
  timer: number;
  Item: any;
  search: any;
  length: any;
  search1: any;

  constructor(private http: HttpService) { }

  ngOnInit() {
  
    this.buildingData=data['default'].data
    console.log(this.buildingData);
  }
  getList(){
    this.http.getist().subscribe((res)=>{
      console.log(res["data"]);
      this.Item=res["data"];
      if(this.search)
      {
        this.Item=res["data"].filter((res:any) => res.min_price > this.search);
      }
      if(this.search1)
      {
        this.Item=res["data"].filter((res:any) => res.name == this.search1);
      }
      this.length=res.data.length;
    });
  }
 
  change(e:any)
  {
    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      this.search=e.target.value;
      this.getList();
    }, 500);
  }
  

 

  }
 


