import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { apiList } from './services/api-list';
import { HttpService } from './services/http.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthModule } from './auth/auth.module';
import { PagesModule } from './pages/pages.module';
import { AuthGuardGuard } from './services/auth-guard.guard';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AuthModule,
    PagesModule
   
  ],
  providers: [apiList, HttpService,AuthGuardGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
