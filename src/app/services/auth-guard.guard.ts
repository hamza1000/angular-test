import { Injectable } from '@angular/core';
import { Router, CanActivate, RouterStateSnapshot, UrlTree, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(next.routeConfig.path.includes('auth')  || next.routeConfig.path == "") {
        console.log(state.url);
        if(!(localStorage.accessToken == null || localStorage.accessToken == undefined || localStorage.accessToken == '')) {
          this.router.navigateByUrl('/pages/dashboard');
          return false;
        } else {
          return true;
        
          

        }
      } else {
        if(localStorage.accessToken !== null) {
          return true;
        } else {
          localStorage.clear();
          this.router.navigateByUrl('/auth/login');
          return false;
        }
      }
  }
}
