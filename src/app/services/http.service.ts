import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { apiList } from './api-list';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  baseUrl: string = environment.baseUrl;

  constructor(  private http: HttpClient,
    private apiList: apiList,) { }
    httpPost(url:string, params): Observable<Object>  {
      return this.http.post(this.baseUrl + this.apiList[url], params);
    }
    getist(){
      return this.http.get<any>("assets/properties.json");
    }
  header() {
    if (localStorage.accessToken != undefined || localStorage.accessToken != null) {
      const headers = new HttpHeaders({
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
        Authorization: JSON.parse(localStorage.getItem('accessToken'))
      });
      const option = {
        headers
      };
      return option;
    }
  }

  formDataHeader() {
    if (
      localStorage.accessToken != undefined ||
      localStorage.accessToken != null
    ) {
      const headers = new HttpHeaders({
        'cache-control': 'no-cache',
        mimeType: 'multipart/form-data',
        Authorization :JSON.parse(localStorage.getItem('accessToken')),
      });
      const option = {
        headers,
      };
      return option;
    }
  }
}
